package com.surecloud.javatechnicalinterview.exam;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExamResultService {

    private final ExamResultDAO examDAO;

    @Autowired
    public ExamResultService(ExamResultDAO examDAO){
        this.examDAO = examDAO;
    }

    public List<ExamResultDTO> findAll(){
        List<ExamResult> examList = examDAO.findAll();
        List<ExamResultDTO> examDTOList = new ArrayList<>();

        for (ExamResult exam: examList) {
            ExamResultDTO examDTO = new ExamResultDTO();
            BeanUtils.copyProperties(exam, examDTO);
            examDTOList.add(examDTO);
        }

        return examDTOList;
    }

    public Optional<ExamResultDTO> findById(String id){
        try{
            UUID.fromString(id).toString().equals(id);
        } catch (IllegalArgumentException exception){
            return Optional.empty();
        }

        Optional<ExamResult> examOptional = Optional.ofNullable(examDAO.findById(id));

        if(examOptional.isPresent()){
            ExamResultDTO examDTO = new ExamResultDTO();
            BeanUtils.copyProperties(examOptional.get(), examDTO);

            return Optional.of(examDTO);
        }else{
            return Optional.empty();
        }
    }

    public Optional<ExamResultDTO> saveExam(ExamResultDTO examDTO){
        try{
            UUID.fromString(examDTO.getId()).toString().equals(examDTO.getId());
        } catch (IllegalArgumentException exception){
            return Optional.empty();
        }
                
        ExamResult exam = new ExamResult();
        BeanUtils.copyProperties(examDTO, exam);
        exam = examDAO.save(exam);
        BeanUtils.copyProperties(exam, examDTO);

        return Optional.of(examDTO);
    }

}

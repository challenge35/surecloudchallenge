package com.surecloud.javatechnicalinterview.exam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/exam")
public class ExamResultController {

    private final ExamResultService examResultService;

    @Autowired
    public ExamResultController(ExamResultService examResultService){
        this.examResultService = examResultService;
    }

    @GetMapping
    public ResponseEntity<List<ExamResultDTO>> findAll(){
        return ResponseEntity.status(HttpStatus.OK).body(examResultService.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ExamResultDTO> findById(@PathVariable String id){
        Optional<ExamResultDTO> exam = examResultService.findById(id);
        if (exam.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(exam.get());
        } else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<ExamResultDTO> saveExam(@RequestBody ExamResultDTO examDTO){
        Optional<ExamResultDTO> examResultDTOOptional = examResultService.saveExam(examDTO);

        if (examResultDTOOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.CREATED).body(examResultDTOOptional.get());
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }
}

package com.surecloud.javatechnicalinterview.exam;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamResultDAO extends JpaRepository<ExamResult, Integer> {

//    Expose an endpoint to return all exam results  OK DONE
//    Expose an endpoint to return a single result identified by its id  OK DONE
//    Expose an endpoint to add a new exam result
//    Include evidence that the features work as intended

    //List<Exam> findAll();
    ExamResult findById(String id);


}
